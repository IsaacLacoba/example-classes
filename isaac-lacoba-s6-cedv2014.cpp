// Suba en un archivo <nombre>-<primer_apellido>-s6-cedv2014.tar.gz la solución (código fuente y Makefile) al siguiente ejercicio:

// Implemente en C++ estándar las clases Vec3 y Triangle, cuyo esqueleto se expone a continuación. El objetivo de la clase Vec3 es proporcionar la funcionalidad básica para manejar información tridimensional, como por ejemplo un punto o un vector en el espacio 3D. La clase Triangle hace uso de Vec3 para definir sus tres vértices.
// Preste especial atención a la clase Triangle, en concreto a la implementación del constructor de copia y al operador de asignador de copia para reservar memoria de manera adecuada.
// Implemente un menú sencillo en la función principal para instanciar triángulos.
// Nota: extienda la implementación de ambas clases si desea incorporar más funcionalidad.

#include <iostream>
#include <vector>
#include "Triangle.h"

#include "Vec3.h"

using namespace std;

class Menu{
  const string welcome_msg =
    "Por favor, introduzca por teclado "
    "un número para seleccionar una opción de las \n"
    "siguientes opciones:\n"
    "\t1) Crear un Triangulo\n"
    "\t2) Ejecutar una demo que prueba el constructor de copia\n"
    "\t3) Ejecutar una demo que prueba el operador =\n"
    "\t4) Salir";

  const string triangle_creation_msg =
    "Por favor, introduzca el valor de los vertices.\n"
    "Se le pedirá que introduzca un numero entero"
    "para cada una de las coordenadas de los tres \nvértices "
    "del triágulo.";

  int option;
  vector<Triangle> triangles;

public:
  Menu(){
    option = 0;
  }
  ~Menu(){}

  void start() {
    cout << "Bienvenido" << endl;

    while(option != 4){
      cout << welcome_msg << endl;
      cin >> option;
      if(option == 1) create_triangle();
      else if(option == 2) demo_copy_const();
      else if(option == 3) demo_operator();
    }
  }

private:
  void create_triangle(){
    Vec3 vertices[3];
    cout << triangle_creation_msg << endl;

    for(int i = 0; i < 3; i++){
      int value = 0;
      cout << "Vertice:" << endl;

      cout << "Introduce x : ";
      cin >>  value;
      vertices[i].setX(value);

      cout << "Introduce y : ";
      cin >>  value;
      vertices[i].setY(value);

      cout  << "Introduce z : ";
      cin >>  value;
      vertices[i].setZ(value);

      cout << endl;

    }
    Triangle triangle(vertices[0], vertices[1], vertices[2]);
    cout << triangle.toString() << endl;
  }

  void demo_copy_const() {
    Triangle triangle1( Vec3(1,1,1), Vec3(2,3,4), Vec3(1,6,9));
    Triangle triangle2( triangle1);

    cout << &triangle1 << " " << &triangle2 << endl
         << "Triangulo 1: "<< triangle1.toString() << endl
         << "Triangulo 2: "<< triangle2.toString() << endl
         << endl;
  }

  void demo_operator() {
    Triangle triangle1( Vec3(1,1,1), Vec3(2,3,4), Vec3(1,6,9));
    Triangle triangle2 = triangle1;

    cout << &triangle1 << " " << &triangle2 << endl
         << "Triangulo 1: "<< triangle1.toString() << endl
         << "Triangulo 2: "<< triangle2.toString() << endl
         << endl;
  }
};

int main(int argc, char *argv[])
{
  Menu menu;
  menu.start();


  return 0;
}
