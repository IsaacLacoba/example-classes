CXX=g++ -std=c++11

CXXFLAGS = -ggdb -I$(HEADERDIR)
LDLIBS =

HEADERDIR=include

TARGETS= Triangle.o Vec3.o

all: isaac-lacoba-s6-cedv2014

isaac-lacoba-s6-cedv2014: $(TARGETS)



clean:
	$(RM)  $(TARGETS) isaac-lacoba-s6-cedv2014
