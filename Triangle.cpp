// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "Triangle.h"
#include "Vec3.h"



Triangle::Triangle (const Vec3 &v1, const Vec3 &v2, const Vec3 &v3) {
  _v1 =  new Vec3(v1);
  _v2 =  new Vec3(v2);
  _v3 =  new Vec3(v3);

}

Triangle::Triangle (const Triangle &t){
  std::cout << "constructor de copia" << std::endl;
  *(this) = t;
}


Triangle::~Triangle (){ delete _v1, _v2, _v3;}

Vec3 Triangle::getV1 () const { return *_v1;}
Vec3 Triangle::getV2 () const { return *_v2;}
Vec3 Triangle::getV3 () const { return *_v3;}

Triangle& Triangle::operator= (const Triangle &t) {
  std::cout << "operador de igual" << std::endl;
  _v1 = new Vec3(t.getV1());
  _v2 = new Vec3(t.getV2());
  _v3 = new Vec3(t.getV3());
}

std::string Triangle::toString() {
  std::stringstream ss;
  ss << "({vertice1:"
     << "(" << _v1->getX() << ", " << _v1->getY() << ", " << _v1->getZ() << ")},"
     << std::endl

     << "{vertice2:"
     << "(" << _v2->getX() << ", " << _v2->getY() << ", " << _v2->getZ() << ")},"
     << std::endl

     << "{vertice3:"
     << "(" << _v3->getX() << ", " << _v3->getY() << ", " << _v3->getZ() << ")})";

  return ss.str();
}
